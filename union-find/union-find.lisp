(in-package :com.lisp-algo.union-find)

(defgeneric create-network (algo-instance n)
  (:documentation "Create a network for the algo-instance"))

(defgeneric union (algo-instance n1 n2)
  (:documentation "Link two nodes in the quick-find network. union_ represent the union operation of the Quick Find Algorithm"))

(defgeneric connected (algo-instance n1 n2)
  (:documentation "Check is there is a path between n1 and n2"))

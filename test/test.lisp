(in-package :com.lisp-algo.test)

(defun do-tests ()
  "Configure lisp-unit and run all tests."
  (setq *print-errors* t)  ; Details tests locations when running tests
  (setq *print-summary* t)  ; Details on tests
  (setq *print-failures* t)  ; Details tests locations when failures
  (run-tests :all :com.lisp-algo.test))

;;;; Define ASDF system
(defsystem "lisp-algo"
    :description "Provide several lisp algorithms"
    :version "0.0.1"
    :depends-on ("lisp-unit")
    :perform (test-op (o s) (symbol-call :com.lisp-algo.test :do-tests))
    :serial t
    :components ((:file "packages")
                 (:module "union-find"
                          :serial t
                          :depends-on ("packages")
                          :components ((:file "union-find")
                                       (:file "quick-find")
                                       (:file "quick-union")
                                       (:file "weighted-quick-union")
                                       (:file "weighted-quick-union-path-compression")))
                 (:module "test"
                          :depends-on ("packages")
                          :components ((:file "test")
                                       (:file "union-find")))))
                                       
                          
